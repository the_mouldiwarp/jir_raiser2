package main

import (
	"fmt"
	"os"

	"bitbucket.org/the_mouldiwarp/jira_raiser2/pkg/readconfig"
	flag "github.com/ogier/pflag"
)

var (
	fWorktype, fProjectkey, fCapopex, fEpiclink string
	fPoints, fIssuetype                         int
)

func getHomeDir() string {
	return os.Getenv("HOME")
}

func main() {
	flag.Parse()
	fmt.Println(fWorktype)
}

func init() {
	config := readconfig.Read(fmt.Sprintf("%s/.jrdefault", getHomeDir()))
	flag.StringVarP(&fWorktype, "worktype", "w", config.Worktype, "Work type (string), example: BAU")
	flag.IntVarP(&fPoints, "points", "p", config.Points, "Point count (int), example: 1")
	flag.StringVarP(&fProjectkey, "projectkey", "k", config.Projectkey, "Project key (string), default: 28500")
	flag.IntVarP(&fIssuetype, "issuetype", "i", config.Issuetype, "Issue type (int), default: 3 (Task)")
	flag.StringVarP(&fCapopex, "capopex", "c", config.Capopex, "Capex or Opex (string), default: OPEX")
	flag.StringVarP(&fEpiclink, "epiclink", "e", config.Epiclink, "Parent Epic ticket name, eg: FHD-424")
}

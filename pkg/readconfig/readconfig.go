package readconfig

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func checkFileExist(p string) {
	_, err := os.Stat(p)
	if err != nil {
		createDefaults(p)
		fmt.Printf("Unable to find a config file, a default has been created: %s\n", p)
	}
}

func parseFile(p string) []byte {

	jsonFile, err := os.Open(p)

	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()

	json, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		panic("Unable to parse file")
	}

	return json
}

// Read takes a path of a file and reads the JSON config into a struct
func Read(p string) *Config {
	checkFileExist(p)
	source := parseFile(p)

	var i Config

	if err := json.Unmarshal([]byte(source), &i); err != nil {
		panic(err)
	}

	return &i
}

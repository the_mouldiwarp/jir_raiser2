package readconfig

import (
	"os"
)

const template = `{
	"worktype": "BAU",
	"points": 1,
	"projectkey": "28500",
	"issuetype": 2,
	"capopex": "OPEX",
	"epiclink": "FHD-424"
}
`

func createDefaults(p string) {
	f, err := os.Create(p)
	if err != nil {
		panic("Unable to create template config file")
	}

	defer f.Close()

	f.Write([]byte(template))
}

package readconfig

// Config holds the defaults for the application
type Config struct {
	Worktype   string `json:"worktype"`
	Points     int    `json:"points"`
	Projectkey string `json:"projectkey"`
	Issuetype  int    `json:"issuetype"`
	Capopex    string `json:"capopex"`
	Epiclink   string `json:"epiclink"`
}
